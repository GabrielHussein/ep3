class Product < ApplicationRecord
  before_destroy :not_referenced_by_any_line_item
  has_many :line_items
  belongs_to :user, optional: true
  mount_uploader :image, ImageUploader
  serialize :image, JSON

  validates :name, :price, :tag, presence: true
  validates :description, length: { maximum: 1000, too_long: "%{count} é o número máximo de caracteres permitido. "}
  validates :name, length: { maximum: 300, too_long: "%{count} é o número máximo de caracteres permitido. "}
  validates :price, length: { maximum: 10 }

  TAG = %w{ Eletrodomésticos Hardware Áudio }

  private
    def not_referenced_by_any_line_item
      unless line_items.empty?
        errors.add(:base, "Line items present")
        throw :abort
      end
  end
end
