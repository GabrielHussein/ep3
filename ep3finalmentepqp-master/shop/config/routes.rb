Rails.application.routes.draw do
  resources :line_items
  resources :carts
  resources :products do
    resources :reviews, except: [:show, :index]
  end
  devise_for :users, controllers:  {
    registrations: 'registrations'
  }
  root 'products#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
